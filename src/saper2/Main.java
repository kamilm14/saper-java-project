package saper2;

import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import java.awt.GridLayout;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import javax.swing.ImageIcon;

/**
 * Created by Piotr Kalembasa i Kamil Mazur
 */
public class Main {

    private JFrame frame;

    private static int fieldsX = 16;
    private static int fieldsY = 16;
    private static final short fieldWidth = 30;
    private static final short fieldHeight = 30;
    private static int minesCount = 20;
    private static Map<String, String> translations = new HashMap<String, String>();

    private int[][] field;
    private boolean isWrong = false;
    private int minesFound = 0;
    private MyButton[][] fieldButtons;
    private ImageIcon mina = new ImageIcon("mina.jpg");

    /**
    * Główna klasa programu
    * @param args program zawiera obsługę parametrów podawanych do niego podczas uruchamiania programu z wiersza poleceń
    */
    public static void main(String[] args) {

        if (args.length == 3) {
            int argWidth = Integer.parseInt(args[0]);
            int argHeight = Integer.parseInt(args[1]);
            int argMines = Integer.parseInt(args[2]);

            if (argWidth > 8 && argWidth < 25) {
                fieldsX = argWidth;
            }

            if (argHeight > 8 && argHeight < 31) {
                fieldsY = argHeight;
            }

            int maxMines = (fieldsX - 1) * (fieldsY - 1);
            if (argMines > 9 && argMines <= maxMines) {
                minesCount = argMines;
            }
        }

        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    Main window = new Main();
                    window.frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }
    
    /**
    * Metoda Main()
    */
    public Main() {
        initialize();
    }
    
    /**
    * Mapa tłumaczeń tekstów wyświetlanych w programie
    */
    
    private void setTranslations() {
        translations.put("game_over", "Porażka!");
        translations.put("win", "Zwycięztwo!");
    }

    private String getTranslation(String key) {
        Object value = translations.get(key);
        return value == null ? null : value.toString();
    }

    /**
    * Metoda sprawdzająca czy w danym polu o współrzędnych x i y można umieścić minę
    * @param x współrzędna x sprawdzanego pola
    * @param y współrzędna y sprawdzanego pola
    * @return zwraca wartość true jeśli można umieścić minę, lub false w przypadku gdy nie można tego zrobić
    */
    private boolean canPutTheMine(int x, int y) {
        if (field[x][y] == -1) {
            return false;
        }
        if (!minesCheck(x, y)) {
            return false;
        }
        for (int i = x - 1; i <= x + 1; i++) {
            if (i >= 0 && i < fieldsX) {
                for (int j = y - 1; j <= y + 1; j++) {
                    if (j >= 0 && j < fieldsY) {
                        if (i != x || j != y) {
                            field[x][y] = -1;
                            if (!minesCheck(i, j)) {
                                field[x][y] = 0;
                                return false;
                            }
                            field[x][y] = 0;
                        }
                    }
                }
            }
        }
        return true;
    }
    
    
    /**
    * Metoda sprawdzająca czy pole w którym chcemy umieścić minę 
    * nie jest miejscem w którym nie dosięgnie go żadna liczba, 
    * oraz czy wstawiona mina nie doprowadzi do takiej sytuacji którejś z postawionych już min.
    * @return zwraca wartość true jeśli można umieścić minę, lub false w przypadku gdy nie można tego zrobić
    */
    private boolean minesCheck(int x, int y) {
        if (countMines(x, y) == 8) {
            return false;
        }
        if ((x == 0 || x == fieldsX - 1) && (y == 0 || y == fieldsY - 1)) {
            if (countMines(x, y) == 3) {
                return false;
            }
        }
        if (x == 0 || x == fieldsX - 1 || y == 0 || y == fieldsY - 1) {
            if (countMines(x, y) == 5) {
                return false;
            }
        }
        return true;
    }
    
    /**
     * Metoda obliczająca liczbę sąsiadujacych min
    */
    private int countMines(int x, int y) {
        int mines = 0;
        for (int i = x - 1; i <= x + 1; i++) {
            if (i >= 0 && i < fieldsX) {
                for (int j = y - 1; j <= y + 1; j++) {
                    if (j >= 0 && j < fieldsY) {
                        if (i != x || j != y) {
                            if (field[i][j] == -1 || field[i][j] == -2) {
                                mines++;
                            }
                        }
                    }
                }
            }
        }
        return mines;
    }
    
    /**
    * Metoda generująca planszę 
    */
    private void generateField() {
        field = new int[fieldsX][fieldsY];
        int i = minesCount;
        Random rand = new Random();
        while (i > 0) {
            int x = rand.nextInt(fieldsX);
            int y = rand.nextInt(fieldsY);
            if (canPutTheMine(x, y)) {
                field[x][y] = -1;
                i--;
            }
        }
        for (i = 0; i < fieldsX; i++) {
            for (int j = 0; j < fieldsY; j++) {
                if (field[i][j] != -1) {
                    field[i][j] = countMines(i, j);
                }
            }
        }
    }

    
    /**
    * Metoda odsłaniająca puste pola które nie sąsiadują z żadnymi ninami
    * @param x współrzędna x sprawdzanego pola
    * @param y współrzędna y sprawdzanego pola
    */
    private void showZeros(int x, int y) {
        if (field[x][y] == 0) {
            for (int i = x - 1; i <= x + 1; i++) {
                if (i >= 0 && i < fieldsX) {
                    for (int j = y - 1; j <= y + 1; j++) {
                        if (j >= 0 && j < fieldsY) {
                            if (i != x || j != y) {
                                fieldButtons[i][j].setText(Integer.toString(field[i][j]));
                                if (fieldButtons[i][j].isEnabled()) {
                                    fieldButtons[i][j].setEnabled(false);
                                    showZeros(i, j);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    /**
    * Metoda sprawdzająca czy naciśnięte pole nie jest miną
    * @param button 
    */
    private void showField(MyButton button) {
        int x = button.getFieldX();
        int y = button.getFieldY();
        if (field[x][y] != -2 && field[x][y] != -3) {
            if (field[x][y] == -1) {
                showAll();
                if (getTranslation("game_over") != null) {
                    JOptionPane.showMessageDialog(frame, getTranslation("game_over"));
                    frame.dispose();
                }
            } else {
                button.setEnabled(false);
                button.setText(Integer.toString(field[x][y]));
                if (field[x][y] == 0) {
                    showZeros(x, y);
                }
            }
        }
    }
    
    /**
    * Metoda która oznacza jako minę pola naścięte przez gracza prawym klawiszem myszy
    * @param button 
    */
    private void setGuess(MyButton button) {
        int x = button.getFieldX();
        int y = button.getFieldY();
        if (button.isEnabled()) {
            if (field[x][y] == -1) {
                field[x][y] = -2;
                button.setText("!");
                minesFound++;
                if (minesFound == minesCount && !isWrong && getTranslation("win") != null) {
                    showAll();
                    JOptionPane.showMessageDialog(frame, getTranslation("win"));
                    frame.dispose();
                }
            } else if (field[x][y] == -2) {
                field[x][y] = -1;
                button.setText("");
                minesFound--;
            } else if (field[x][y] == -3) {
                field[x][y] = countMines(x, y);
                isWrong = false;
                button.setText("");
            } else {
                field[x][y] = -3;
                button.setText("!");
                isWrong = true;
            }
        }
    }

    
    /**
    * Metoda ta odsłania całą planszę w przypadku porażki
    */
    private void showAll() {
        for (int i = 0; i < fieldsX; i++) {
            for (int j = 0; j < fieldsY; j++) {
                String text = Integer.toString(field[i][j]);
                if (text.equals("-1") || text.equals("-2")) {
                    fieldButtons[i][j].setText("");
                    fieldButtons[i][j].setIcon(mina);
                } else if (text.equals("-3")) {
                    text = Integer.toString(countMines(i, j));
                    fieldButtons[i][j].setText(text);
                }
            }
        }
    }
    
    /**
    * Metoda rysuje pole gry i przygotowuje obsluge klikniec myszy na odpowiednich polach
    */       
    private void initialize() {
        setTranslations();

        frame = new JFrame();
        frame.setSize(fieldsX * fieldWidth, fieldsY * fieldHeight);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().setLayout(new GridLayout(fieldsY, fieldsX, 0, 0));
        frame.setLocationRelativeTo(null);
        generateField();

        fieldButtons = new MyButton[fieldsX][fieldsY];
        for (int j = 0; j < fieldsY; j++) {
            for (int i = 0; i < fieldsX; i++) {
                fieldButtons[i][j] = new MyButton(i, j);
                fieldButtons[i][j].addMouseListener(new MouseListener() {
                    public void mouseClicked(MouseEvent e) {
                        int button = e.getButton();
                        if (button == MouseEvent.BUTTON1) {
                            showField((MyButton) e.getSource());
                        } else if (button == MouseEvent.BUTTON3) {
                            setGuess((MyButton) e.getSource());
                        }
                    }

                    @Override
                    public void mouseEntered(MouseEvent e) {
                    }

                    @Override
                    public void mouseExited(MouseEvent e) {
                    }

                    @Override
                    public void mousePressed(MouseEvent e) {
                    }

                    @Override
                    public void mouseReleased(MouseEvent e) {
                    }
                });
                frame.getContentPane().add(fieldButtons[i][j]);
            }
        }

    }

}
