package saper2;


import javax.swing.JButton;

    /**
    * Klasa rozszerzająca klasę JButton o dodatkową informację - położenie na planszy
    */
public class MyButton extends JButton {
	private final int fieldX;
	private final int fieldY;
	
	public MyButton(int x, int y){
		super();
		fieldX=x;
		fieldY=y;
	}
	
	public int getFieldX(){
		return fieldX;
	}
	
	public int getFieldY(){
		return fieldY;
	}
}
